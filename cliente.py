#!/usr/bin/python
# Coded by: Alisson Machado
# Contact: alisson.machado@responsus.com.br
#
# fonte: https://blog.4linux.com.br/socket-em-python/

import socket

#'127.0.0.1'
ip = input('digite o ip de conexao: ')
print (ip)
port = 7000
addr = ((ip,port))
client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client_socket.connect(addr)

mensagem = str(input("digite uma mensagem para enviar ao servidor\n"))

client_socket.send(mensagem.encode())
print('mensagem enviada')
client_socket.close()
